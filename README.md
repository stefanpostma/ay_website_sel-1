1. Edit the metadata.json with: scriptname, transactions and parameters
2. Edit the pom.xml with: scriptname
3. Double check the library versions used in the pom.xml
 
For help with parameters look at: https://userhappiness.atlassian.net/wiki/spaces/YSDK/pages/645464065/Parameters+in+Selenium